# JeuxMaths

Placer le fichier mathGame.sty dans texfm/latex

Pour compiler :

$ pdflatex -synctex=1 -interaction=nonstopmode --shell-escape fichier.tex

## License

<https://creativecommons.org/licenses/by-sa/4.0/>

